<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::pattern('id', '[0-9]+');

Route::pattern('event_id', '[0-9]+');

Route::pattern('item', 'new|[0-9]+');

Route::pattern('users_events', 'users|events');

Route::pattern('social', 'facebook|vkontakte|instagram');

Route::pattern('voteable', 'events|photos|videos');

Route::pattern('me_id', 'me|[0-9]+');

Route::get('callback/{social}', 'SocialAuthController@callback');

Route::get('callback/referral', 'UserController@referral');

Route::post('{voteable}/vote', 'VoteController@vote');

//Route::get('/test', 'UserController@test');

Route::group(['middleware' => 'auth:api'], function () {

    Route::group(['prefix' => 'profile'], function () {

        Route::get('/', 'UserController@profile');

        Route::post('update', 'UserController@update');

    });

    Route::group(['prefix' => 'code'], function () {

        Route::post('send', 'SmsController@send');

        Route::post('check', 'SmsController@check');

    });

    Route::get('status', 'SocialAuthController@status');

    Route::group(['middleware' => ['has.phone', 'filled']], function () {

        Route::group(['prefix' => 'events'], function () {

            Route::get('/', 'EventController@index');

            Route::get('categories', 'EventController@categories');

            Route::post('store', 'EventController@store');

            Route::group(['prefix' => '{event_id}'], function () {

                Route::get('/', 'EventController@view');

                Route::get('accept/{id}', 'EventUserController@accept');

                Route::get('remove/{id}', 'EventUserController@remove');

                Route::get('users', 'EventUserController@index');

                Route::get('join', 'EventController@join');

                Route::get('leave', 'EventController@leave');

            });

        });

        Route::group(['prefix' => 'friends'], function () {

            Route::get('/', 'FriendshipController@index');

            Route::get('add/{id}', 'FriendshipController@add');

            Route::get('request/{id}', 'FriendshipController@request');

        });

        Route::group(['prefix' => 'videos'], function () {

            Route::post('upload', 'VideoController@upload');

        });

        Route::get('users/{me_id}', 'UserController@view');

        Route::get('users/invited', 'UserController@invited');

        Route::get('{users_events}/{id}/photos', 'PhotoController@index');

        Route::post('{users_events}/photos/upload', 'PhotoController@upload');

        //Route::post('{users_events}/videos/upload', 'VideoController@upload');

        Route::post('photos/delete', 'PhotoController@delete');

        Route::post('photos/avatar/{id}', 'PhotoController@avatar');

        Route::post('bind/{social}', 'UserController@bind');

        Route::get('messages', 'MessageController@index');

        Route::get('messages/{id}', 'MessageController@view');

        Route::get('messages/delete/{id}', 'MessageController@delete');

        Route::get('messages/suggests', 'MessageController@suggests');

    });

});
/*
Route::post('register', 'Auth\RegisterController@register');
Route::middleware('web')->get('redirect/facebook', 'SocialAuthController@redirectFacebook');
Route::middleware('web')->get('redirect/vkontakte', 'SocialAuthController@redirectVkontakte');
*/