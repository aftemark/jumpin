<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom
    |--------------------------------------------------------------------------
    */

    'no_access' => 'Нет доступа.',
    'sms_content' => 
        "Ваш проверочный код:\n
        :sms_code\n
        - JumpIn",
    'phone_less_error' => 'Телефон уже подтвержден.',
    'no_phone_error' => 'Телефон не подтвержден.',
    'phone_occupied' => 'Телефон уже занят.',
    'unfilled_error' => 'Профиль не заполнен.',
    'sms_code_error' => 'Неверный смс код.',
    'session_code_error' => 'Попробуйте еще раз.',
    'wrong_location' => 'Неверное местоположение.',
    'event_requirement_error' => 'Вы не подходите под требования события.',
    'event_already_error' => 'Заявка уже отправлена.',
    'event_late_error' => 'Событие уже прошло.',
    'already_approved' => 'Заявка уже подтверждена.',
    'friend_requested' => 'Запрос уже отправлен.',
    'cant_request' => 'Запрос не может быть отправлен этому пользователю.',
    'no_request' => 'Такого запроса не существует.',
    'wrong_token' => 'Неверный токен.',
    'social_binded' => 'Социальная сеть уже привязана.',
    'photos_limit' => 'Превышен лимит фотографий.',

    'sporty_name' => 'Активный отдых',
    'sporty_description' => 'спорт, совместные выезды',
    'cultural_name' => 'Культурные мероприятия',
    'cultural_description' => 'кино, театры',
    'light_name' => 'Лайт-тусовки',
    'light_description' => 'культурные тусовки дома',
    'hard_name' => 'Хард-тусовки',
    'hard_description' => 'для взрослых мальчиков и девочек',
    'residence_name' => 'Проживание',
    'residence_description' => 'впишу к себе пожить',
    'hitchhike_name' => 'Попутчики',
    'hitchhike_description' => 'довезу, возьму с собой в путешествие',
    '1to1_name' => 'Один на один',
    '1to1_description' => 'романтика на двоих',
    
];
