<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'nickname' => 'user1',
            'first_name' => 'first1',
            'last_name' => 'last1',
            'birthday' => '1993-09-01',
            'sex' => 1,
            'city' => 'city',
            'phone' => '79875027811'
        ]);
        DB::table('users')->insert([
            'nickname' => 'user2',
            'first_name' => 'first2',
            'last_name' => 'last2',
            'birthday' => '1993-09-02',
            'sex' => 2,
            'city' => 'city2',
            'phone' => '79875027812'
        ]);
        DB::table('users')->insert([
            'nickname' => 'user3',
            'first_name' => 'first3',
            'last_name' => 'last3',
            'birthday' => '1993-09-03',
            'sex' => 1,
            'city' => 'city3',
            'phone' => '79875027813'
        ]);
        DB::table('users')->insert([
            'nickname' => 'user4',
            'first_name' => 'first4',
            'last_name' => 'last4',
            'birthday' => '1993-09-04',
            'sex' => 1,
            'city' => 'city4',
            'phone' => '79875027814'
        ]);
        DB::table('users')->insert([
            'nickname' => 'user5',
            'first_name' => 'first5',
            'last_name' => 'last5',
            'birthday' => '1993-09-05',
            'sex' => 2,
            'city' => 'city',
            'phone' => '79875027815'
        ]);

        DB::table('friendships')->insert([
            'sender_id' => 1,
            'sender_type' => 'App\User',
            'recipient_id' => 2,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
        DB::table('friendships')->insert([
            'sender_id' => 1,
            'sender_type' => 'App\User',
            'recipient_id' => 3,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
        DB::table('friendships')->insert([
            'sender_id' => 1,
            'sender_type' => 'App\User',
            'recipient_id' => 4,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
        DB::table('friendships')->insert([
            'sender_id' => 1,
            'sender_type' => 'App\User',
            'recipient_id' => 5,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
        DB::table('friendships')->insert([
            'sender_id' => 1,
            'sender_type' => 'App\User',
            'recipient_id' => 6,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
        DB::table('friendships')->insert([
            'sender_id' => 1,
            'sender_type' => 'App\User',
            'recipient_id' => 7,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);

        DB::table('friendships')->insert([
            'sender_id' => 2,
            'sender_type' => 'App\User',
            'recipient_id' => 3,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
        DB::table('friendships')->insert([
            'sender_id' => 2,
            'sender_type' => 'App\User',
            'recipient_id' => 4,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
        DB::table('friendships')->insert([
            'sender_id' => 2,
            'sender_type' => 'App\User',
            'recipient_id' => 5,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
        DB::table('friendships')->insert([
            'sender_id' => 2,
            'sender_type' => 'App\User',
            'recipient_id' => 6,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
        DB::table('friendships')->insert([
            'sender_id' => 2,
            'sender_type' => 'App\User',
            'recipient_id' => 7,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);

        DB::table('friendships')->insert([
            'sender_id' => 3,
            'sender_type' => 'App\User',
            'recipient_id' => 4,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
        DB::table('friendships')->insert([
            'sender_id' => 3,
            'sender_type' => 'App\User',
            'recipient_id' => 5,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
        DB::table('friendships')->insert([
            'sender_id' => 3,
            'sender_type' => 'App\User',
            'recipient_id' => 6,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
        DB::table('friendships')->insert([
            'sender_id' => 3,
            'sender_type' => 'App\User',
            'recipient_id' => 7,
            'recipient_type' => 'App\User',
            'status' => 1
        ]);
    }
}
