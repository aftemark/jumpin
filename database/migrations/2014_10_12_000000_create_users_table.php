<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nickname')->unique()->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->date('birthday')->nullable();
            $table->tinyInteger('sex')->nullable();
            $table->string('city')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            //$table->boolean('social_hidden')->default(false);
            $table->boolean('show_phone')->default(false);
            //$table->string('copy_city')->nullable();
            //$table->boolean('videos_events')->default(false);
            $table->boolean('hide_age')->default(false);
            //$table->boolean('contacts_hidden')->default(false);
            $table->integer('first_social_id')->unsigned();
            $table->integer('referral_id')->unsigned()->nullable();
            $table->boolean('filled')->default(false);
            $table->boolean('premium')->default(false);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('first_social_id')->references('id')->on('users');
            $table->foreign('referral_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
