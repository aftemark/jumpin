<?php

return [
    'social_services' => [
        'facebook' => [
            'fields' => [
                'id' => 'id',
                'email' => 'email',
                'first_name' => 'first_name',
                'last_name' => 'last_name',
                'birthday' => 'birthday',
                'location' => 'location',
                'gender' => 'sex'
            ],
            'url' => 'https://www.facebook.com/',
            'id_field' => 'id'
        ],
        'vkontakte' => [
            'fields' => [
                'email' => 'email',
                'nickname' => 'nickname',
                'sex' => 'sex',
                'country' => 'country',
                'city' => 'city',
                'bdate' => 'birthday'
            ],
            'url' => 'https://vk.com/id',
            'id_field' => 'id'
        ],
        'instagram' => [
            'fields' => [
                'username' => 'nickname',
                'full_name' => 'full_name',
            ],
            'url' => 'https://www.instagram.com/',
            'id_field' => 'username'
        ]
    ],
    'categories' => ['sporty', 'cultural', 'light', 'hard', 'residence', 'hitchhike', '1to1'],
    'categories_mime' => '.png',
    'categories_fills_prefix' => '-fill',
    'filters' => ['old', 'popular', 'unpopular'],
    'default_paginate_number' => 20,
    'photos_paginate_number' => 15,
    'model_photos_paginate_number' => 6,
    'users_photos_max_number' => 5,
    'events_photos_max_number' => 12,
    'voteable' => [
        'events' => 'App\Event',
        'photos' => 'App\Photo',
        'videos' => 'App\Video'
    ],
    'videos' => [
        'binaries' => [
            'ffmpeg.binaries'  => env('FFMPEG_PATH', '/opt/local/ffmpeg/bin/ffmpeg'),
            'ffprobe.binaries' => env('FFPROBE_PATH', '/opt/local/ffmpeg/bin/ffprobe'),
            'timeout' => env('FFMPEG_TIMEOUT', 3600),
            'ffmpeg.threads' => env('FFMPEG_THREADS', 12)
        ],
        'thumbnail' => [
            'width'  => env('THUMBNAIL_IMAGE_WIDTH', 720),
            'height' => env('THUMBNAIL_IMAGE_HEIGHT', 480),
        ]
    ]
    /*'categories' => [
        'sporty' => [
            'name' => 'Активный отдых',
            'description' => 'спорт, совместные выезды'
        ],
        'cultural' => [
            'name' => 'Культурные мероприятия',
            'description' => 'кино, театры'
        ],
        'light' => [
            'name' => 'Лайт-тусовки',
            'description' => 'культурные тусовки дома'
        ],
        'hard' => [
            'name' => 'Хард-тусовки',
            'description' => 'для взрослых мальчиков и девочек'
        ],
        'residence' => [
            'name' => 'Проживание',
            'description' => 'впишу к себе пожить'
        ],
        'hitchhike' => [
            'name' => 'Попутчики',
            'description' => 'довезу, возьму с собой в путешествие'
        ],
        '1to1' => [
            'name' => 'Один на один',
            'description' => 'романтика на двоих'
        ]
    ],*/
];