<?php

namespace App;

use Sofa\Eloquence\Eloquence;
use Hootlex\Friendships\Traits\Friendable;
use Tymon\JWTAuth\Contracts\JWTSubject as JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Eloquence;
    use Friendable;
    use \HighIdeas\UsersOnline\Traits\UsersOnlineTrait;

    protected $fillable = [
        'nickname',
        'first_name',
        'last_name',
        'birthday',
        'sex'
    ];

    protected $hidden = [
        'email',
        'phone',
        'birthday',
        'show_phone',
        'hide_age',
        'first_social_id',
        'referral_id',
        'filled',
        'premium',
        'remember_token',
        'password',
        'created_at',
        'updated_at'
    ];

    protected $searchableColumns = [
        'first_name' => 10,
        'last_name' => 10,
        'nickname' => 5
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function first_social()
    {
        return $this->belongsTo('App\UserSocialAccount');
    }

    public function referral()
    {
        return $this->belongsTo('App\User', 'referral_id');
    }

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function entries()
    {
        return $this->hasMany('App\EventUser');
    }

    public function socials()
    {
        return $this->hasMany('App\UserSocialAccount');
    }

    public function photos()
    {
        return $this->morphMany('App\Photo', 'model');
    }

    public function sent_messages()
    {
        return $this->hasMany('App\Message', 'sender_id');
    }

    public function received_messages()
    {
        return $this->hasMany('App\Message', 'recipient_id');
    }

    public function referred()
    {
        return $this->hasMany('App\User', 'referral_id');
    }
}