<?php

namespace App\Ratchet;

use JWTAuth;
use App\User;
use App\Message;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Guzzle\Http\Message\RequestInterface;

class ChatSocket implements MessageComponentInterface
{
    protected $clients;
    protected $users;
    protected $connections;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
        $this->users = [];
        $this->connections = [];
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $request = $conn->WebSocket->request->getQuery()->toArray();
        if (isset($request['token']) && !is_null($user = JWTAuth::setToken($request['token'])->toUser())) {
            $this->users[$user->id][$conn->resourceId] = $conn;
            $this->connections[$conn->resourceId] = $user->id;
            $this->clients->attach($conn);
            echo "New connection! ({$conn->resourceId})\n";
        }

        /*$this->clients->attach($conn);
        $this->users[$conn->resourceId] = $conn;
        $session = (new SessionManager(App::getInstance()))->driver();
        $cookies = $conn->WebSocket->request->getCookies();
        $laravelCookie = urldecode($cookies[Config::get('session.cookie')]);
        $idSession = Crypt::decrypt($laravelCookie);
        $session->setId($idSession);
        $conn->session = $session;*/

        /*$conn->session->start();
        $idUser = $conn->session->get(Auth::getName());
        if (!isset($idUser))
            $conn->close();

        $conn->session->save();
        $user = User::find($idUser);
        echo json_encode($user);
        if (in_array($user->id, $this->users))
            $this->users[] = $user->id;
        if (in_array($conn->resourceId, $this->users[$user->id]))
            $this->users[$user->id][] = [$conn->resourceId => $conn];
        echo "New connection! ({$conn->resourceId})\n";*/


        /*$user = Auth::user();
        if (!$user)
            $conn->close();

        $this->clients->attach($conn);
        echo json_encode($conn);
        if (in_array($user->id, $this->users))
            $this->users[] = $user->id;
        if (in_array($conn->resourceId, $this->users[$user->id]))
            $this->users[$user->id][] = [$conn->resourceId => $conn];

        echo "New connection! ({$conn->resourceId})\n";*/
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $data = json_decode($msg);

        if (isset($data->user_id) && isset($data->message)) {
            if (isset($this->users[$data->user_id]) && $data->user_id != ($sender_id = $this->connections[$from->resourceId]))
                foreach ($this->users[$data->user_id] as $connection) {
                    $connection->send(json_encode([
                        'user_id' => $sender_id,
                        'message' => $data->message,
                    ]));
                    Message::create([
                        'sender_id' => $sender_id,
                        'receiver_id' => $data->user_id,
                        'body' => $data->message
                    ]);
                    echo $data->message . ' - Sent!';
                }
        }

        /*echo $msg;
        if (isset($data->token) && isset($data->user_id) && isset($data->message)) {
            $user = JWTAuth::setToken($data->token)->toUser();
            if (!is_null($user)) {
                if (!isset($this->users[$user->id][$from->resourceId])) {
                    $this->users[$user->id][$from->resourceId] = $from;
                    $this->connections[$from->resourceId] = $user->id;
                }

                if (isset($this->users[$data->user_id]) && $user->id != $data->user_id)
                    foreach ($this->users[$data->user_id] as $connection) {
                        $connection->send($data->message);
                        echo $data->message . ' - Sent!';
                    }
            } else
                $from->close();

        }
        echo json_encode($this->users);
        echo json_encode($this->connections);*/

        //if (isset($data->token)) {
        //    $user = JWTAuth::toUser($data->token);
        //    echo json_encode($user);
        //}
        /*$user = Auth::guard('api')->user();
        $data = json_decode($msg);
        if ($user && isset($data['user_id']) && isset($data['message'])) {
            $otherUser = User::find($data['user_id']);
            if (!is_null($otherUser) && $user->id != $otherUser->id && isset($this->users[$otherUser->id]))
                foreach ($this->users[$otherUser->id] as $connection)
                    $connection->send($data->message);
        }*/

        /*$numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send($msg);
            }
        }*/
    }

    public function onClose(ConnectionInterface $conn)
    {
        // The connection is closed, remove it, as we can no longer send it messages
        //$this->clients->detach($conn);

        //echo "Connection {$conn->resourceId} has disconnected\n";

        $this->clients->detach($conn);
        unset($this->users[$this->connections[$conn->resourceId]][$conn->resourceId]);
        unset($this->connections[$conn->resourceId]);
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}