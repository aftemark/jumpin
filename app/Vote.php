<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = [
        'user_id',
        'voteable_id',
        'voteable_type'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function voteable()
    {
        return $this->morphTo();
    }
}
