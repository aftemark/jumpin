<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class UserFilled
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('api')->user()->filled)
            return $next($request);
        else
            return response()->json(['error' => __('messages.unfilled_error')], 403);
    }
}
