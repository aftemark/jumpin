<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class UserHasPhone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!is_null(Auth::guard('api')->user()->phone))
            return $next($request);
        else
            return response()->json(['error' => __('messages.no_phone_error')], 403);
    }
}
