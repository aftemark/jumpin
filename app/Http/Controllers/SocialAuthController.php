<?php

namespace App\Http\Controllers;

use Auth;
use Config;
use JWTAuth;

use Illuminate\Http\Request;
use App\Http\Traits\HelperTrait;

class SocialAuthController extends Controller
{
    use HelperTrait;

    public function redirectSocial($social)
    {
        return Socialite::driver($social)->redirect();
    }

    public function callbackSocial($social)
    {
        return response()->json([
            'token' => JWTAuth::fromUser($this->createOrGetUser(Socialite::driver($social)->user(), $social))
        ]);
    }

    public function status()
    {
        $user = Auth::guard('api')->user();
        if (!$user->phone)
            $return = ['view' => 'phone'];
        else if (!$user->filled)
            $return = ['view' => 'profile'];
        else
            $return = ['view' => 'hello'];

        return response()->json($return);
    }

    public function callback(Request $request, $social)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
        $fields = Config::get('constants.social_services');

        $response = $this->getSocialModel($social, implode(',', array_keys($fields[$social]['fields'])), $request->token);
        if (isset($response['error']))
            return response()->json($response, 403);
        
        return response()->json([
            'token' => JWTAuth::fromUser($this->createOrGetUser($response, $social))
        ]);
    }
}