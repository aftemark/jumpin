<?php

namespace App\Http\Controllers;

use Auth;

use Illuminate\Http\Request;

class SmsController extends Controller
{
    public function send(Request $request)
    {
        if (Auth::guard('api')->user()->phone)
            return response()->json(['error' => __('messages.phone_less_error')], 403);

        $this->validate($request, [
            'phone' => 'required|phone:AUTO,RU'
        ]);

        $sms_code = strval(mt_rand(1000, 9999));
        $phone = phone($request->phone, 'RU');

        if (\App\User::where('phone', $phone)->exists())
            return response()->json(['phone' => __('messages.phone_occupied')], 422);

        $client = new \Zelenin\SmsRu\Api(new \Zelenin\SmsRu\Auth\ApiIdAuth(env('SMS_APP_ID')));
        $sms = new \Zelenin\SmsRu\Entity\Sms($phone, __('messages.sms_content', compact('sms_code')));
        $client->smsSend($sms);

        session(compact(['sms_code', 'phone']));
    }

    public function check(Request $request)
    {
        if (Auth::guard('api')->user()->phone)
            return response()->json(['error' => __('messages.phone_less_error')], 403);
        
        $this->validate($request, [
            'sms_code' => 'required|integer|numeric|between:1000,9999'
        ]);

        if (!$request->session()->has('phone') || !$request->session()->has('sms_code'))
            return response()->json(['error' => __('messages.session_code_error')], 403);

        if ($request->sms_code != $request->session()->pull('sms_code', false))
            return response()->json(['sms_code' => __('messages.sms_code_error')], 422);

        $user = Auth::guard('api')->user();
        $user->phone = phone($request->session()->pull('phone'));
        $user->save();
    }
}