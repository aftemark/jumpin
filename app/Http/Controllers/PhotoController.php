<?php

namespace App\Http\Controllers;

use Config;

use App\User;
use App\Event;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PhotoController extends Controller
{
    public function index($users_events, $id)
    {
        $paginate = Config::get('constants.photos_paginate_number');
        if ($users_events == 'users')
            return User::findOrFail($id)->photos()->paginate($paginate);
        else if ($users_events == 'events')
            return Event::findOrFail($id)->photos()->paginate($paginate);
    }

    public function upload(Request $request, $users_events)
    {
        $maxPhotos = Config::get('constants.' . $users_events . '_photos_max_number');
        $rules = [
            'photos' => 'required|array|max:' . $maxPhotos,
            'photos.*' => 'required|image|mimes:jpeg,png|max:5000'
        ];

        if ($users_events == 'users') {
            $this->validate($request, $rules);
            $entity = $request->user();
        } else if ($users_events == 'events') {
            $this->validate($request, $rules + [
                'id' => 'required|exists:' . $users_events . ',id'
            ]);
            $entity = $request->user()->events()->findOrFail($request->id);
        }
        $photosCount = $entity->photos->count();

        foreach ($request->photos as $requestPhoto) {
            if ($photosCount >= $maxPhotos)
                return response()->json(['photos' => __('messages.photos_limit')], 422);
            $photo = new Photo;
            $photo->filename = $requestPhoto->store('photos/' . $users_events . '/' . $entity->id);
            $photo->model()->associate($entity);
            $photo->save();
            $photosCount++;
        }
    }

    public function delete(Request $request, $id)
    {
        $photo = Photo::findOrFail($id);

        if (($photo->model_type == 'App/User' && $photo->model_id == $request->user()->id) || ($photo->model_type == 'App/Event' && $photo->model->user_id == $request->user()->id))
            Storage::delete($photo->filename);
        else
            return response()->json(['error' => __('messages.no_access')], 403);
    }

    public function avatar(Request $request, $id)
    {
        $photo = Photo::findOrFail($id);

        if (!$photo->avatar) {
            if (($photo->model_type == 'App/User' && $photo->model_id == $request->user()->id) || ($photo->model_type == 'App/Event' && $photo->model->user_id == $request->user()->id)) {
                foreach ($photo->model->photos as $modelPhoto)
                    $modelPhoto->update([
                        'avatar' => $modelPhoto->id == $id
                    ]);
            } else
                return response()->json(['error' => __('messages.no_access')], 403);
        }
    }
}
