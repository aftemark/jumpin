<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Config;

use App\User;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class MessageController extends Controller
{
    public function index(Request $request)
    {
        $userId = $request->user()->id;
        $perPage = Config::get('constants.default_paginate_number');
        $page = $request->input('page', 1);

        $rawMessages = Message::select('*')->from(DB::raw(
            "(
                SELECT *,`sender_id` as `u`
                FROM `messages` WHERE `recipient_id` = {$userId}
                union
                SELECT *,`recipient_id` as `u`
                FROM `messages` WHERE `sender_id` = {$userId}
                order by `created_at` desc
            ) as `sub`"))->where(function ($query) use ($userId) {
            $query->where('deleted_by', '!=', $userId)->orWhereNull('deleted_by');
        })->groupBy('u');

        if ($request->has('q')) {
            $rawMessages = Message::whereIn('id', $rawMessages->pluck('id'));
            $messages = collect($rawMessages->where('sender_id', $userId)->search($request->q, ['recipient.first_name', 'recipient.last_name'])->get()->load('sender')->load('recipient'))->
                merge($rawMessages->where('recipient_id', $userId)->search($request->q, ['sender.first_name', 'sender.last_name'])->get()->load('sender')->load('recipient'))->sortByDesc('created_at');
        } else
            $messages = $rawMessages->orderByDesc('created_at')->get()->load('sender')->load('recipient');

        $messages = new LengthAwarePaginator(
            array_slice($messages->toArray(), ($page * $perPage) - $perPage, $perPage, true),
            count($messages),
            $perPage,
            $page,
            ['path' => $request->url(), 'query' => $request->query()]
        );

        return response()->json($messages);
    }

    public function view(Request $request, $id)
    {
        User::findOrFail($id);
        $userId = $request->user()->id;
        $perPage = Config::get('constants.default_paginate_number');
        $page = $request->input('page', 1);

        $messages = collect(Message::whereIn('sender_id', [$id, $userId])->whereIn('recipient_id', [$id, $userId])->where(function ($query) use ($userId) {
            $query->where('deleted_by', '!=', $userId)->orWhereNull('deleted_by');
        })->orderByDesc('created_at')->get());
        $messages = new LengthAwarePaginator(
            array_slice($messages->toArray(), ($page * $perPage) - $perPage, $perPage, true),
            count($messages),
            $perPage,
            $page,
            ['path' => $request->url(), 'query' => $request->query()]
        );

        return response()->json($messages);
    }

    public function suggests(Request $request)
    {
        $perPage = Config::get('constants.default_paginate_number');
        $user = $request->user();
        $page = $request->input('page', 1);
        $friendships = $user->getAllFriendships();

        $conversations = Message::select('*')->from(DB::raw(
            "(
                SELECT *,`sender_id` as `u`
                FROM `messages` WHERE `recipient_id` = {$user->id}
                union
                SELECT *,`recipient_id` as `u`
                FROM `messages` WHERE `sender_id` = {$user->id}
                order by `created_at` desc
            ) as `sub`"))->groupBy('u');

        $friendshipIds = collect($friendships->pluck('sender_id'))->merge($friendships->pluck('recipient_id'))->unique();
        $users = User::whereIn('id', $friendshipIds->diff(collect(collect($conversations->pluck('sender_id'))->merge(collect($conversations->pluck('recipient_id'))))->unique())->except($user->id));

        if ($request->has('q'))
            $users->search($request->q);

        return response()->json(
            new LengthAwarePaginator(
                array_slice($users->get()->load('first_social')->toArray(), ($page * $perPage) - $perPage, $perPage, true),
                count($users),
                $perPage,
                $page,
                ['path' => $request->url(), 'query' => $request->query()]
        ));
    }

    public function delete($id)
    {
        User::findOrFail($id);
        $user = Auth::guard('api')->user();

        Message::whereIn('sender_id', [$id, $user->id])->whereIn('recipient_id', [$id, $user->id])->whereNull('deleted_by')->update(['deleted_by' => $user->id]);
        Message::whereIn('sender_id', [$id, $user->id])->whereIn('recipient_id', [$id, $user->id])->whereNotNull('deleted_by')->where('deleted_by', '!=', $user->id)->delete();
    }
}