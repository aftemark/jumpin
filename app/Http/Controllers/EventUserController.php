<?php

namespace App\Http\Controllers;

use Auth;
use Config;

use App\User;
use App\Event;
use App\EventUser;
use Illuminate\Http\Request;

class EventUserController extends Controller
{
    public function index(Request $request, $event_id)
    {
        $this->validate($request, [
            't' => 'required|in:requests,members'
        ]);

        $usersIDs = $request->t == 'members' ? Event::findOrFail($event_id)->users()->whereApproved(true)->pluck('user_id') : Auth::guard('api')->user()->events()->findOrFail($event_id)->users()->whereApproved(false)->pluck('user_id');

        $users = User::whereIn('id', $usersIDs);

        if ($request->has('q'))
            $users->search($request->q);

        $users = $users->with('first_social')->orderByDesc('updated_at')->paginate(Config::get('constants.default_paginate_number'));
        return response()->json($users);
    }

    public function accept($event_id, $id)
    {
        $eventUser = EventUser::whereUserId($id)->whereEventId($event_id)->firstOrFail();

        if ($eventUser->event->user_id != Auth::guard('api')->user()->id)
            return response()->json(['error' => __('messages.no_access')], 403);

        if ($eventUser->approved)
            return response()->json(['error' => __('messages.already_approved')], 403);

        $eventUser->approved = true;
        $eventUser->save();
    }

    public function remove($event_id, $id)
    {
        $eventUser = EventUser::whereUserId($id)->whereEventId($event_id)->firstOrFail();

        if ($eventUser->event->user_id != Auth::guard('api')->user()->id)
            return response()->json(['error' => __('messages.no_access')], 403);
        
        $eventUser->delete();
    }
}