<?php

namespace App\Http\Controllers;

use Auth;
use Config;
use Carbon\Carbon;

use App\User;
use App\UserSocialAccount;
use Illuminate\Http\Request;
use App\Http\Traits\HelperTrait;

class UserController extends Controller
{
    use HelperTrait;

    public function profile()
    {
        $fields = Config::get('constants.social_services');
        $user = Auth::guard('api')->user();
        $user->makeVisible(['email', 'phone', 'birthday', 'show_phone', 'hide_age', 'filled', 'premium', 'created_at', 'updated_at']);
        foreach ($user->socials as $social)
            if (!is_null($social->page_id))
                $social->url = $fields[$social->provider]['url'] . $social->page_id;

        return response()->json($user);
    }

    public function bind(Request $request, $social)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
        $fields = Config::get('constants.social_services');

        if ($request->user()->socials()->whereProvider($social)->exists())
            return response()->json(['error' => __('messages.social_binded')], 403);

        $response = $this->getSocialModel($social, implode(',', array_keys($fields[$social]['fields'])), $request->token);
        if (isset($response['error']))
            return response()->json($response, 403);

        if (UserSocialAccount::whereProvider($social)->where('provider_user_id', $response['id'])->exists())
            return response()->json(['error' => __('messages.social_binded')], 403);

        UserSocialAccount::create([
            'provider_user_id' => $response['id'],
            'provider' => $social,
            'user_id' => $request->user()->id,
            'page_id' => $response[$fields[$social]['id_field']]
        ]);
    }

    public function view($me_id)
    {
        $me = Auth::guard('api')->user();
        $meOrUser = $me_id != 'me' && $me_id != $me->id;
        $user = $meOrUser ? User::findOrFail($me_id) : $me;
        $user->load('photos');
        foreach ($user->photos as $photo) {
			$photo->likes = $photo->votes()->whereRating(1)->count();
			$photo->dislikes = $photo->votes()->whereRating(-1)->count();
		}
        if (!$user->filled)
            return response()->json(['error' => __('messages.no_access')], 403);

        $now = Carbon::now();
        $constants = Config::get('constants');

        $data = [];
        $data['events_count'] = [
            'own' => $user->events()->where('date_time', '>=', $now)->count(),
            'archived' => $user->events()->where('date_time', '<', $now)->count(),
            'participant' => $user->entries()->count(),
        ];
        foreach ($user->socials as $social)
            if (!is_null($social->page_id) && !$social->hide)
                $social->url = $constants['social_services'][$social->provider]['url'] . $social->page_id;
        if (!$user->hide_age) {
            $data['age'] = $this->ageString(Carbon::now()->diffInYears(Carbon::parse($user->birthday)));
            $user->makeVisible(['birthday']);
		}
        if ($user->show_phone)
            $user->makeVisible(['phone']);
        $data['user_status'] = $meOrUser ? ($me->isFriendWith($user) ? 'friend' : 'not_friend') : 'me';

        return response()->json([
            'user' => $user,
            'data' => $data
        ]);
    }

    public function update(Request $request)
    {
        $user = Auth::guard('api')->user();
        $socials = Config::get('constants.social_services');

        $this->validate($request, [
            'first_name' => 'required|string|min:2|max:255',
            'last_name' => 'required|string|min:2|max:255',
            'nickname' => 'required|string|min:2|max:255|unique:users,nickname,' . $user->id,
            'birthday' => 'required|date_format:d.m.Y',
            'sex' => 'required|in:1,2',
            'city' => 'required|string|min:2|max:255',
        ]);

        $user->fill($request->all());
        $user->birthday = Carbon::parse($request->birthday)->toDateString();

        $user->city = $this->getLocation($request->city, 'locality');
        if ($user->city == NULL)
            return response()->json(['city' => __('messages.wrong_location')], 422);

        if ($user->premium)
            foreach (['show_phone', 'hide_age'/*, 'social_hidden', 'videos_events', 'contacts_hidden'*/] as $field)
                $user[$field] = $request->has($field);

        if (!$user->filled)
            $user->filled = true;

        $user->save();
        
        foreach (array_keys($socials) as $social) {
            $socialAccount = $user->socials()->whereProvider($social);
            if ($socialAccount->exists()) {
                $socialAccount = $socialAccount->first();
                $socialAccount->hide = $request->has($social);
                $socialAccount->save();
            }
        }
    }

    public function referral(Request $request)
    {
        $constants = Config::get('constants');
        $this->validate($request, [
            'referral_id' => 'required|exists:users,id',
            'provider' => 'required|in:' . implode(array_keys($constants['social_services']), ','),
            'provider_user_id' => 'required',
        ]);

        $user = Auth::guard('api')->user();
        $referral = User::find($request->referral_id);
        $userSocial = UserSocialAccount::where('provider', $request->provider)->where('provider_user_id', $request->provider_user_id);

        if ($user !== NULL) {
            if (!$user->isFriendWith($referral))
                $user->befriend($referral);
            $action = 'logged';
        } else if ($userSocial->exists()) {
            if (!$user->isFriendWith($referral))
                $request->session()->put('befriend_id', $request->referral_id);
            $action = 'login';
        } else {
            $request->session()->put('referral_id', $request->referral_id);
            $action = 'register';
        }

        return response()->json(compact('action'));
    }
    
    public function invited(Request $request)
    {
        $user = $request->user();
        $users = $user->referred();

        if ($request->has('q'))
            $users->search($request->q);

        return response()->json(['users' => $users->paginate(Config::get('constants.default_paginate_number'))]);
    }

    /*public function test()
    {
        $response = ['token' => JWTAuth::fromUser(\App\User::find(1))];

        return response()->json($response);
    }*/
}