<?php

namespace App\Http\Controllers;

use Auth;
use Config;
use Session;
use Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\User;
use App\Vote;
use App\Event;
use App\EventUser;
use App\Http\Traits\HelperTrait;

class EventController extends Controller
{
    use HelperTrait;

    public function view($event_id)
    {
        $event = Event::findOrFail($event_id);
        $event->load('user');

        $data = [
            'photos' => $event->photos()->paginate(Config::get('constants.model_photos_paginate_number')),
            'likes' => $event->votes()->whereRating(true)->count(),
            'dislikes' => $event->votes()->whereRating(false)->count(),
            'users_count' => $event->users()->count(),
            'users' => $event->users()->with('user')->whereApproved(true)->paginate(Config::get('constants.default_paginate_number'))
        ];

        return response()->json([
            'event' => $event,
            'data' => $data,
        ]);
    }

    public function store(Request $request)
    {
        $categories = Config::get('constants.categories');
        $this->validate($request, [
            'id' => 'numeric|integer|exists:events,id',
            'name' => 'required|string|max:191',
            'category' => 'required|in:' . implode(',', $categories),
            'description' => 'required',
            'date_time' => 'required|date_format:Y-m-d H:i:s',
            'location' => 'required|string|max:191',
            'gender' => 'in:1,2',
        ]);

        $user = Auth::guard('api')->user();
        $event = $request->has('id') ? $user->events()->findOrFail($request->id) : new Event;

        $event->fill($request->all());

        if (!$event->id && $user->premium)
            $event->premium = true;

        $event->user_id = $user->id;
        $event->social_hidden = $request->has('social_hidden');
        $event->location = $this->getLocation($request->location, 'street_number');
        if ($event->location == NULL || (!$event->premium && strpos($event->location, $user->city) === false))
            return response()->json(['location' => __('messages.wrong_location')], 422);

        $event->save();
    }

    public function join($event_id)
    {
        $event = Event::findOrFail($event_id);
        if (Carbon::now()->diffInSeconds(Carbon::parse($event->date_time), false) > 0)
            return response()->json(['error' => __('messages.event_late_error')], 403);

        $user = Auth::guard('api')->user();
        if ($event->users()->whereUserId($user->id)->exists())
            return response()->json(['error' => __('messages.event_already_error')], 403);

        if ($event->gender !== NULL && $user->sex != $event->gender)
            return response()->json(['error' => __('messages.event_requirement_error')], 403);

        $deleted = $event->users()->withTrashed()->whereUserId($user->id);
        if (is_null($deleted))
            EventUser::create([
                'user_id' => $user->id,
                'event_id' => $event->id
            ]);
        else {
            $deleted->restore();
            $deleted->approved = false;
            $deleted->save();
        }
    }

    public function leave($event_id)
    {
        $event = Event::findOrFail($event_id);
        $user = Auth::guard('api')->user();
        $eventUser = $event->users()->whereUserId($user->id)->firstOrFail();

        $eventUser->delete();
    }

    public function index(Request $request)
    {
        $constants = Config::get('constants');
        $user = $request->user();
        $this->validate($request, [
            't' => 'required|in:all,self,user',
            'self_scope' => 'required_if:t,self|in:my,approved,requested',
            'user_scope' => 'required_if:t,user|in:his,past,requested',
            'user_id' => 'required_if:t,user|integer|numeric|exists:user,id',
            'c' => 'in:' . implode(',', $constants['categories']),
            'f' => 'in:' . implode(',', $constants['filters']),
            'time_frame' => 'in:current,past',
            'date_start' => 'date',
            'date_end' => 'date',
        ]);

        $now = Carbon::now();
        $countCurrent = true;

        if ($request->t == 'all') {
            $events = Event::select('*')->search('"' . $user->city . '"', ['location']);
            if (!$request->has('time_frame'))
                $events = $events->where('date_time', '>=', $now);
            if ($request->has('c')) {
                $events = $events->whereCategory($request->c);
                $top = clone $events->wherePremium(true);
                $events = $events->wherePremium(false);
                $return['category'] = [
                    'name' => $request->c,
                    'count' => $events->count()
                ];
                //dd(Event::whereIn('id', $top->pluck('id'))->withCount('likes')->withCount('dislikes')->get());
            }
        } else if ($request->t == 'self') {
            if ($request->self_scope == 'my') {
                $events = $user->events();
            } else if ($request->self_scope == 'approved') {
                $events = Event::whereIn('id', $user->entries()->whereApproved(true)->pluck('event_id'));
            } else if ($request->self_scope == 'requested') {
                $events = Event::where('date_time', '>=', $now)->whereIn('id', $user->entries()->whereApproved(false)->pluck('event_id'));
            }
        } else if ($request->t == 'user') {
            $targetUser = User::find($request->user_id);
            if ($request->user_scope == 'his') {
                $events = $targetUser->events();
            } else {
                $events = Event::whereIn('id', $targetUser->entries()->whereApproved(true)->pluck('event_id'))->where('date_time', $request->user_scope == 'past' ? '<' : '>=', $now);
                $countCurrent = $request->user_scope == 'requested';
            }
        }

        if ($request->has('time_frame') && ($request->t == 'all' || ($request->t == 'self' && ($request->self_scope == 'my' || $request->self_scope == 'approved')) || ($request->t == 'user' && $request->user_scope == 'his'))) {
            $events->where('date_time', $request->time_frame == 'current' ? '>=' : '<', $now);
            $countCurrent = $request->time_frame == 'current';
        }

        if ($request->has('date_start'))
            $events->where('date_time', '>=', $request->date_start);
        if ($request->has('date_end'))
            $events->where('date_time', '<=', $request->date_end);

        if ($request->has('q'))
            $events->search($request->q);

        if ($request->t == 'all' && !$request->has('c')) {
            $events->orderByDesc('date_time')->orderByDesc('premium');
            if (isset($top))
                $top->orderByDesc('date_time');
        }

        if (($request->has('f') && $request->f != 'old') || ($request->t == 'all' && !$request->has('c'))) {
            $sort = 'desc';
            if (($request->has('f') && $request->f == 'unpopular'))
                $sort = 'asc';
            $events = Event::whereIn('id', $events->pluck('id'))->orderBy('likes_count', $sort);
            if (isset($top))
                $top = Event::whereIn('id', $top->pluck('id'))->orderBy('likes_count', $sort);
        }

        if ($request->has('f')) {
            if ($request->f == 'old') {
                $events->orderBy('date_time', 'asc');
                if (isset($top))
                    $top->orderBy('date_time', 'asc');
            } else {
                $events->orderBy('likes_count', $request->f == 'popular' ? 'desc' : 'asc');
                if (isset($top))
                    $top->orderBy('likes_count', $request->f == 'popular' ? 'desc' : 'asc');
            }
        }

        $return['events'] = $events->withCount('likes')->withCount('dislikes')->paginate($constants['default_paginate_number'], ['*'], 'events_page');
        dd($top->withCount('likes')->withCount('dislikes')->paginate($constants['default_paginate_number'], ['*'], 'top_page'));
        if (isset($top))
            $return['top'] = $top->withCount('likes')->withCount('dislikes')->paginate($constants['default_paginate_number'], ['*'], 'top_page');
        foreach ($return['events'] as $event) {
            $event->user_count = $countCurrent ? $event->users()->whereApproved(true)->count() : $event->users()->withTrashed()->whereApproved(true)->count();
            $event->avatar = $event->photos()->whereAvatar(true)->first();
            if ($user->entries()->where('event_id', $event->id)->exists())
                $event->participant = true;
        }

        $categories = $constants['categories'];
        foreach ($categories as $category) {
            $return['categories'][$category] = [
                'name' => __('messages.' . $category . '_name'),
                'fill' => Storage::url('photos/categories/' . $category . $constants['categories_fills_prefix'] . $constants['categories_mime'])
            ];
        }
        $return['premium_fill'] = Storage::url('photos/categories/premium' . $constants['categories_fills_prefix'] . $constants['categories_mime']);

        return response()->json($return);
    }

    public function categories(Request $request)
    {
        $this->validate($request, [
            't' => 'required|in:current,past'
        ]);

        $constants = Config::get('constants');
        $now = Carbon::now();
        $comparison = $request->t == 'current' ? '>=' : '<';

        $return = ['categories' => []];

        foreach ($constants['categories'] as $category) {
            $return['categories'][] = [
                'category' => $category,
                'name' => __('messages.' . $category . '_name'),
                'description' => __('messages.' . $category . '_description'),
                'count' => Event::whereCategory($category)->where('date_time', $comparison, $now)->search($request->user()->city, ['location'])->count(),
                'image' => Storage::url('photos/categories/' . $category . $constants['categories_mime'])
            ];
        }

        return response()->json($return);
    }
}
