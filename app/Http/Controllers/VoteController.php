<?php

namespace App\Http\Controllers;

use Auth;
use Config;
use Illuminate\Http\Request;

use App\Vote;

class VoteController extends Controller
{
    public function vote($voteable, Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|numeric|exists:events,id',
            'r' => 'integer|numeric|in:1,-1'
        ]);

        $vote = Vote::firstOrCreate([
            'user_id' => Auth::guard('api')->user()->id, 'voteable_id' => $request->id, 'voteable_type' => Config::get('constants.voteable.' . $voteable)
        ]);
        $vote->rating = $request->has('r') ? $request->r : NULL;
        $vote->save();
    }
}
