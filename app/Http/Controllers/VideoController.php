<?php

namespace App\Http\Controllers;

use File;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\Coordinate;
use Illuminate\Http\Request;

use App\Video;

class VideoController extends Controller
{
    public function upload(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'event_id' => 'required|exists:events,id',
            'video' => 'required|mimes:mp4,mov,m4v,avi,webm|max:999999'
        ]);
        $user = $request->user();
        $entity = $user->events()->findOrFail($request->event_id);

        $event_id = 2;
        $video = $request->file('video');
        $fileName = $video->hashName();
        $binaries = config('constants.videos.binaries');
        $filePath = $request->file('video')->store('videos/1');
        $fullFilePath = storage_path('app/public/' . $filePath);
        $duration = floor(FFProbe::create($binaries)->format($fullFilePath)->get('duration') / 2);

        $cutThumbnailsPath = 'thumbnails/videos/' . $event_id;
        $thumbnailsPath = storage_path('app/public/' . $cutThumbnailsPath);
        if (!File::exists($thumbnailsPath))
            File::makeDirectory($thumbnailsPath);
        $thumbnailName = '/' . $fileName . '-thumbnail.jpg';
        FFMpeg::create($binaries)->open($fullFilePath)->frame(Coordinate\TimeCode::fromSeconds($duration))->save($thumbnailsPath . $thumbnailName);

        $newImage = imagecreatefromjpeg($thumbnailsPath . $thumbnailName);

        $width = imagesx($newImage);
        $height = imagesy($newImage);
        $newWidth = config('constants.videos.thumbnail.width');
        $newHeight = config('constants.videos.thumbnail.height');

        $tmp = imagecreatetruecolor($newWidth, $newHeight);

        imagecopyresampled($tmp, $newImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        imageline($tmp, imagesx($tmp) / 2, imagesy($tmp) / 2, imagesx($tmp) / 2, imagesy($tmp) / 2, IMG_COLOR_BRUSHED);
        imagejpeg($tmp, $thumbnailsPath . $thumbnailName, 75);

        $video = new Video;
        $video->fill($request->all());
        $video->filename = $filePath;
        $video->thumbnail_filename = $cutThumbnailsPath . $thumbnailName;
        $video->save();
    }
}
