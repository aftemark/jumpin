<?php

namespace App\Http\Controllers;

use Auth;
use Config;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class FriendshipController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, [
            't' => 'required|in:all,online,requests,suggests'
        ]);
        $user = Auth::guard('api')->user();
        $constants = Config::get('constants');
        $perPage = $constants['default_paginate_number'];

        if ($request->t == 'all') {
            $friends = $user->getFriends()->load('first_social');
            if ($request->has('q'))
                $friends = User::whereIn('id', $friends->pluck('id'))->search($request->q)->get();
        } else if ($request->t == 'online') {
            $friends = $user->getFriends()->load('first_social');
            $friends = $friends->filter(function ($user) {
                return $user->isOnline();
            });
            if ($request->has('q'))
                $friends->search($request->q);
        } else if ($request->t == 'requests') {
            $friends = collect([]);
            foreach ($user->getPendingFriendships() as $friendship)
                $friends->push($user->id == $friendship->sender_id ? $friendship->recipient->load('first_social') : $friendship->sender->load('first_social'));
        } else if ($request->t == 'suggests') {
            $suggested[] = $user->id;
            $suggests = collect([]);
            $userFriends = collect($user->getFriends())->keyBy('id');
            while ($userFriends->count()) {
                $randomFriend = $userFriends->random();
                $userFriends->forget($randomFriend->id);
                foreach ($randomFriend->getFriends()->load('first_social') as $newSuggest) {
                    if (!in_array($newSuggest->id, $suggested) && !$user->isFriendWith($newSuggest)) {
                        $newSuggest->mutualsCount = $user->getMutualFriendsCount($newSuggest);
                        $suggested[] = $newSuggest->id;
                        $suggests->push($newSuggest);
                        if ($suggests->count() == $perPage)
                            break;
                    }
                }
            }

            return response()->json($suggests);
        }

        $page = $request->input('page', 1);

        $return = new LengthAwarePaginator(
            array_slice($friends->toArray(), ($page * $perPage) - $perPage, $perPage, true),
            count($friends),
            $perPage,
            $page,
            ['path' => $request->url(), 'query' => $request->query()]
        );

        return response()->json($return);
    }

    public function add($id)
    {
        $user = Auth::guard('api')->user();
        $otherUser = User::findOrFail($id);
        if ($user->hasFriendRequestFrom($otherUser) || $user->hasSentFriendRequestTo($otherUser))
            return response()->json(['error' => __('messages.friend_requested')], 403);

        if (!$user->befriend($otherUser))
            return response()->json(['error' => __('messages.cant_request')], 403);
    }

    public function remove($id)
    {
        $user = Auth::guard('api')->user();
        $otherUser = User::findOrFail($id);
        if (!$user->isFriendWith($otherUser))
            return response()->json(['error' => __('messages.friend_requested')], 403);

        $user->unfriend($otherUser);
    }

    public function request(Request $request, $id)
    {
        $this->validate($request, [
            't' => 'required|in:accept,deny'
        ]);

        $user = Auth::guard('api')->user();
        $sender = User::findOrFail($id);
        if (!$user->hasFriendRequestFrom($sender))
            return response()->json(['error' => __('messages.no_request')], 403);

        if ($request->t == 'accept')
            $user->acceptFriendRequest($sender);
        else
            $user->denyFriendRequest($sender);
    }
}
