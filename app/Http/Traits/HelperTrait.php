<?php

namespace App\Http\Traits;

use Config;
use Session;
use Facebook;
use GoogleMaps;
use Carbon\Carbon;

use App\User;
use App\UserSocialAccount;
use Mbarwick83\Instagram\Instagram;

trait HelperTrait
{
    public function createOrGetUser($providerUser, $provider)
    {
        $fields = Config::get('constants.social_services');
        $userFields = [];
        foreach ($providerUser as $providerUserFieldName => $providerUserFieldValue)
            $userFields[isset($fields[$provider]['fields'][$providerUserFieldName]) ? $fields[$provider]['fields'][$providerUserFieldName] : $providerUserFieldName] = $providerUserFieldValue;

        $account = UserSocialAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser['id'])
            ->first();

        if ($account) {
            if (Session::has('befriend_id')) {
                $beFriend = User::find(Session::pull('befriend_id'));
                if (!$account->user->isFriendWith($beFriend))
                    $account->user->befriend($beFriend);
            }

            return $account->user;
        }

        $account = new UserSocialAccount([
            'provider_user_id' => $providerUser['id'],
            'provider' => $provider,
            'social_name' => isset($userFields['full_name']) ? $userFields['full_name'] : $userFields['first_name'] . ' ' . $userFields['last_name'],
            'page_id' => $providerUser[$fields[$provider]['id_field']]
        ]);

        $nickname = NULL;
        if (!empty($userFields['nickname']) && !User::whereNickname($userFields['nickname'])->exists())
            $nickname = $userFields['nickname'];

        if (isset($userFields['sex'])) {
            if ($userFields['sex'] == 'female')
                $userFields['sex'] = 1;
            if ($userFields['sex'] == 'male')
                $userFields['sex'] = 2;
        }

        if ($provider == 'vkontakte')
            $userFields['location'] = $userFields['country']['title'] . ' ' . $userFields['city']['title'];

        $birthday = NULL;
        if (isset($userFields['birthday']))
            $birthday = is_object($userFields['birthday']) ? $providerUser->getBirthday()->format('Y-m-d') : $userFields['birthday'];

        if (isset($userFields['full_name']))
            list($userFields['first_name'], $userFields['last_name']) = explode(" ", $userFields['full_name'], 2);

        $user = User::create([
            'email' => isset($userFields['email']) ? $userFields['email'] : NULL,
            'first_name' => $userFields['first_name'],
            'last_name' => $userFields['last_name'],
            'birthday' => $birthday != NULL ? Carbon::parse($birthday)->toDateString() : NULL,
            'nickname' => $nickname,
            'sex' => isset($userFields['sex']) ? $userFields['sex'] : NULL,
            'city' => isset($userFields['location']) ? $this->getLocation($userFields['location'], 'locality') : NULL,
            'first_social_id' => $account->id
        ]);

        $account->user()->associate($user);
        $account->save();

        if (Session::has('referral_id')) {
            $referral = User::find(Session::pull('referral_id'));
            $referral->befriend($user);

            $user->associate($referral);
            $user->save();
        }

        return $user;
    }

    public function getSocialModel($social, $fields, $token)
    {
        switch ($social) {
            case 'vkontakte':
                $api = new \ATehnix\VkClient\Client;
                $api->setDefaultToken($token);

                $response = $api->request('users.get', ['fields' => $fields]);
                if (isset($response["response"]))
                    $return = array_shift($response["response"]);
                else
                    $return =  ['error' => $response];
                break;
            case 'facebook':
                try {
                    $response = Facebook::get('/me?fields=' . $fields, $token);
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    return ['error' => $e->getMessage()];
                }
                $return = $response->getGraphUser();
                break;
            case 'instagram':
                $instagram = new Instagram;
                $response = $instagram->get('v1/users/self', ['access_token' => $token]);
                if ($response["meta"]["code"] == 200)
                    $return = $response["data"];
                else
                    $return = ['error' => $response["meta"]["error_message"]];
                break;
        }

        return $return;
    }

    public function getLocation($address, $accuracy)
    {
        $response = \GoogleMaps::load('geocoding')
            ->setParam ([
                //'place_id' =>'ChIJ0fAKpGIAQUERAanLoexs7bo'
                'address' => $address
            ])
            ->get();

        $response = json_decode($response);

        if ($response->status != 'OK')
            return NULL;
        $result = array_shift($response->results);
        $firstComponent = array_shift($result->address_components);
        if (!in_array($accuracy, $firstComponent->types))
            return NULL;

        return $result->formatted_address;
    }

    public function ageString($age)
    {
        $age = abs($age);
        $t1 = $age % 10;
        $t2 = $age % 100;
        return $age . ' ' . ($t1 == 1 && $t2 != 11 ? "год" : ($t1 >= 2 && $t1 <= 4 && ($t2 < 10 || $t2 >= 20) ? "года" : "лет"));
    }
}