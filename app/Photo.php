<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'filename'
    ];

    public function model()
    {
        return $this->morphTo();
    }

    public function votes()
    {
        return $this->morphMany('App\Vote', 'voteable');
    }
}
