<?php

namespace App;

use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use Eloquence;

    protected $fillable = ['sender_id', 'recipient_id', 'body'];

    protected $hidden = ['deleted_by'];

    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id');
    }

    public function recipient()
    {
        return $this->belongsTo('App\User', 'recipient_id');
    }
}
