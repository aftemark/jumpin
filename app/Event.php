<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Event extends Model
{
    use Eloquence;
    
    protected $fillable = [
        'name',
        'category',
        'description',
        'date_time',
        'location',
        'gender'
    ];

    protected $searchableColumns = [
        'name',
        'description',
        'location',
    ];

    public function users()
    {
        return $this->hasMany('App\EventUser');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function votes()
    {
        return $this->morphMany('App\Vote', 'voteable');
    }

    public function likes()
    {
        return $this->morphMany('App\Vote', 'voteable')->whereRating(1);
    }

    public function dislikes()
    {
        return $this->morphMany('App\Vote', 'voteable')->whereRating(-1);
    }

    public function photos()
    {
        return $this->morphMany('App\Photo', 'model');
    }
}
