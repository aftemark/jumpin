[Дизайн/API приложения](fish.jpg?raw=true)

get /callback/{facebook|vkontakte|instagram}?token=токен соц сети
результат: 'token' => {JWT token}


далее методы для авторизованых пользователей т.е. с хэдером Authorization: Bearer {JWT token}
get /profile

результат: модель пользователя
"id": 1,
"nickname": "vasyan2",
"first_name": "Vasya",
"last_name": "Petrov",
"birthday": "1990-12-14",
"sex": 2,
"city": "Санкт-Петербург, Россия",
"phone": "+7 987 502-78-17",
"email": null,
"social_hidden": 0,
"show_phone": 0,
"copy_city": null,
"videos_events": 0,
"hide_age": 0,
"contacts_hidden": 0,
"first_social_id": 1,
"filled": 1,
"socials": [
    {
        "id": 1,
        "user_id": 1,
        "provider_user_id": "428393759",
        "provider": "vkontakte",
        "hide": 0,
        "page_id": "428393759",
        "created_at": "2017-05-18 15:25:13",
        "updated_at": "2017-06-19 11:56:09",
        "url": "https://vk.com/id428393759"
    }
]
если filled == 0 то не разрешать дальше экрана редактирования личной информации, станет 1 после сохранения
	
	
post /profile/update

'first_name' => 'required|string|max:255',
'last_name' => 'required|string|max:255',
'nickname' => 'required|string|max:255|unique:users',
'birthday' => 'required|date_format:d.m.Y',
'sex' => 'required|in:1,2',
'city' => 'required|string|max:255'


get /status (для обращений после /callback/{facebook|vkontakte} или /code/check)

результат: 'view' => '{phone|profile|hello}'
если phone то на экран подтверждения телефона
если profile то на экран оичной информации
если hello то дальше


post /code/send

'phone' => {телефон}
пока только для РФ номеров
результат: HTTP Status code 200


post /code/check

'sms_code' => {4х значный код}
результат: HTTP Status code 200


get /events

t=all|self|user
с=sporty|cultural|light|hard|residence|hitchhike|1to1 (работает только если t=all)
self_scope=my|approved|requested (обязательное поле если t=self)
user_scope=his|past|requested (обязательное поле если t=user)
user_id=id пользователя (обязательное поле если t=user)
f=old|popular|unpopular
time_frame=current|past
date_start={YYYY-MM-DD}
date_end={YYYY-MM-DD}
q=поиск

результат:
"events": {
    "current_page": 1,
    "data": [
        {
            "id": 1,
            "name": "event",
            "user_id": 1,
            "category": "cultural",
            "description": "event",
            "date_time": "2017-12-01 00:00:00",
            "location": "Москва",
            "social_hidden": 0,
            "gender": null,
            "created_at": "2017-06-01 00:00:00",
            "updated_at": "2017-06-01 00:00:00",
            "relevance": 12,
            "user_count": 2,
            "likes": 1,
            "dislikes": 0,
             "avatar": {
                 "id": 2,
                 "model_id": 1,
                 "model_type": "App\\Event",
                 "filename": "photos/events/1/pXnxgeJnZwisRhalDOPEXy6JWvVQpggKpsmcpAqJ.jpeg",
                 "avatar": 1,
                 "created_at": "2017-06-15 14:50:33",
                 "updated_at": "2017-06-15 14:50:33"
             }
        }
    ],
    "from": 1,
    "last_page": 1,
    "next_page_url": null,
    "path": "http://jumpin.dev/api/events",
    "per_page": 10,
    "prev_page_url": null,
    "to": 1,
    "total": 1
}
"fills": {
    "sporty": "http://jumpin.dev/storage/photos/categories/sporty-fill.png",
    "cultural": "http://jumpin.dev/storage/photos/categories/cultural-fill.png",
    "light": "http://jumpin.dev/storage/photos/categories/light-fill.png",
    "hard": "http://jumpin.dev/storage/photos/categories/hard-fill.png",
    "residence": "http://jumpin.dev/storage/photos/categories/residence-fill.png",
    "hitchhike": "http://jumpin.dev/storage/photos/categories/hitchhike-fill.png",
    "1to1": "http://jumpin.dev/storage/photos/categories/1to1-fill.png"
}
если отображение по категориям то добавляется ключ category
"category": {
    "name": "cultural",
    "count": 1
},


post /events/store

'first_name' => 'required|string|max:255',
'last_name' => 'required|string|max:255',
'nickname' => 'required|string|max:255|unique:users',
'birthday' => 'required|date_format:d.m.Y',
'sex' => 'required|in:1,2',
'city' => 'required|string|max:255'


post /events/vote

'id' => 'required|integer|numeric|exists:events,id',
'r' => 'integer|numeric|in:1,-1'
результат: HTTP Status code 200


get /events/categories?t={current|past}

результат:
{
    "categories": [
        {
            "category": "sporty",
            "name": "Активный отдых",
            "description": "спорт, совместные выезды",
            "count": 1,
            "image": "http://jumpin.dev/storage/app/public/photos/categories/sporty.jpg"
        },
        {
            "category": "cultural",
            "name": "Культурные мероприятия",
            "description": "кино, театры",
            "count": 1,
            "image": "http://jumpin.dev/storage/app/public/photos/categories/cultural.jpg"
        },
        {
            "category": "light",
            "name": "Лайт-тусовки",
            "description": "культурные тусовки дома",
            "count": 0,
            "image": "http://jumpin.dev/storage/app/public/photos/categories/light.jpg"
        },
        {
            "category": "hard",
            "name": "Хард-тусовки",
            "description": "для взрослых мальчиков и девочек",
            "count": 0,
            "image": "http://jumpin.dev/storage/app/public/photos/categories/hard.jpg"
        },
        {
            "category": "residence",
            "name": "Проживание",
            "description": "впишу к себе пожить",
            "count": 0,
            "image": "http://jumpin.dev/storage/app/public/photos/categories/residence.jpg"
        },
        {
            "category": "hitchhike",
            "name": "Попутчики",
            "description": "довезу, возьму с собой в путешествие",
            "count": 0,
            "image": "http://jumpin.dev/storage/app/public/photos/categories/hitchhike.jpg"
        },
        {
            "category": "1to1",
            "name": "Один на один",
            "description": "романтика на двоих",
            "count": 0,
            "image": "http://jumpin.dev/storage/app/public/photos/categories/1to1.jpg"
        }
    ]
}


get /friends?t={all|online|requests|suggests|}&q={поиск только для all|online}

результат:
"current_page": 1,
"data": [
    {
        "id": 3,
        "nickname": null,
        "first_name": "Archibald",
        "last_name": "Fox",
        "birthday": "1989-10-10",
        "sex": 2,
        "city": null,
        "phone": "+7 999 210-88-92",
        "email": null,
        "social_hidden": 0,
        "show_phone": 0,
        "copy_city": null,
        "videos_events": 0,
        "hide_age": 0,
        "contacts_hidden": 0,
        "filled": 0,
        "first_social": {
           "id": 1,
           "user_id": 1,
           "provider_user_id": "428393759",
           "provider": "vkontakte",
           "hide": 0,
           "page_id": "428393759",
           "created_at": "2017-05-18 15:25:13",
           "updated_at": "2017-06-19 11:56:09"
        }
    },
    {
        "id": 5,
        "nickname": "user2",
        "first_name": "first2",
        "last_name": "last2",
        "birthday": "1993-09-02",
        "sex": 2,
        "city": "city2",
        "phone": "79875027812",
        "email": null,
        "social_hidden": 0,
        "show_phone": 0,
        "copy_city": null,
        "videos_events": 0,
        "hide_age": 0,
        "contacts_hidden": 0,
        "filled": 0,
        "first_social": {
            "id": 1,
            "user_id": 1,
            "provider_user_id": "428393759",
            "provider": "vkontakte",
            "hide": 0,
            "page_id": "428393759",
            "created_at": "2017-05-18 15:25:13",
            "updated_at": "2017-06-19 11:56:09"
        }
    }
],
"from": 1,
"last_page": 1,
"next_page_url": null,
"path": "http://jumpin.dev/api/friends",
"per_page": 10,
"prev_page_url": null,
"to": 2,
"total": 2


get /friends/add/{user_id}

результат: HTTP Status code 200


get /friends/remove/{user_id}

результат: HTTP Status code 200


get /friends/request/{user_id}?t=accept|deny

результат: HTTP Status code 200


get /events/{id}

результат: данные события
"event": {
	"id": 1,
	"name": "event",
	"user_id": 1,
	"category": "cultural",
	"description": "event",
	"date_time": "2017-12-01 00:00:00",
	"location": "Москва",
	"social_hidden": 0,
	"gender": null,
	"created_at": "2017-06-01 00:00:00",
	"updated_at": "2017-06-01 00:00:00",
	"user": {
		"id": 1,
		"nickname": "vasyan2",
		"first_name": "Vasya",
		"last_name": "Petrov",
		"birthday": "1990-12-14",
		"sex": 2,
		"city": "0",
		"phone": "+7 987 502-78-17",
		"email": null,
		"social_hidden": 0,
		"show_phone": 0,
		"copy_city": null,
		"videos_events": 0,
		"hide_age": 0,
		"contacts_hidden": 0,
		"filled": 1,
        "first_social": {
           "id": 1,
           "user_id": 1,
           "provider_user_id": "428393759",
           "provider": "vkontakte",
           "hide": 0,
           "page_id": "428393759",
           "created_at": "2017-05-18 15:25:13",
           "updated_at": "2017-06-19 11:56:09"
        }
	}
},
"data": {
	"photos": {
		"current_page": 1,
		"data": [
			{
				"id": 2,
				"model_id": 1,
				"model_type": "App\\Event",
				"filename": "photos/events/1/pXnxgeJnZwisRhalDOPEXy6JWvVQpggKpsmcpAqJ.jpeg",
				"avatar": 0,
				"created_at": "2017-06-15 14:50:33",
				"updated_at": "2017-06-15 14:50:33"
			}
		],
		"from": 1,
		"last_page": 1,
		"next_page_url": null,
		"path": "http://jumpin.dev/api/events/1",
		"per_page": 6,
		"prev_page_url": null,
		"to": 1,
		"total": 1
	},
	"likes": 1,
	"dislikes": 0,
	"users_count": 3,
	"users": {
		"current_page": 1,
		"data": [
			{
				"id": 1,
				"user_id": 1,
				"event_id": 1,
				"approved": 1,
				"user": {
					"id": 1,
					"nickname": "vasyan2",
					"first_name": "Vasya",
					"last_name": "Petrov",
					"birthday": "1990-12-14",
					"sex": 2,
					"city": "0",
					"phone": "+7 987 502-78-17",
					"email": null,
					"social_hidden": 0,
					"show_phone": 0,
					"copy_city": null,
					"videos_events": 0,
					"hide_age": 0,
					"contacts_hidden": 0,
					"filled": 1
				}
			},
			{
				"id": 2,
				"user_id": 3,
				"event_id": 1,
				"approved": 1,
				"user": {
					"id": 3,
					"nickname": null,
					"first_name": "Archibald",
					"last_name": "Fox",
					"birthday": "1989-10-10",
					"sex": 2,
					"city": null,
					"phone": "+7 999 210-88-92",
					"email": null,
					"social_hidden": 0,
					"show_phone": 0,
					"copy_city": null,
					"videos_events": 0,
					"hide_age": 0,
					"contacts_hidden": 0,
					"filled": 0
				}
			}
		],
		"from": 1,
		"last_page": 1,
		"next_page_url": null,
		"path": "http://jumpin.dev/api/events/1",
		"per_page": 10,
		"prev_page_url": null,
		"to": 2,
		"total": 2
	}
}


get /events/{id}/users?t={requests|members}&q={поиск}

результат:
"current_page": 1,
"data": [
	{
		"id": 3,
		"nickname": null,
		"first_name": "Archibald",
		"last_name": "Fox",
		"birthday": "1989-10-10",
		"sex": 2,
		"city": null,
		"phone": "+7 999 210-88-92",
		"email": null,
		"social_hidden": 0,
		"show_phone": 0,
		"copy_city": null,
		"videos_events": 0,
		"hide_age": 0,
		"contacts_hidden": 0,
		"filled": 0,
        "first_social": {
           "id": 1,
           "user_id": 1,
           "provider_user_id": "428393759",
           "provider": "vkontakte",
           "hide": 0,
           "page_id": "428393759",
           "created_at": "2017-05-18 15:25:13",
           "updated_at": "2017-06-19 11:56:09"
        }
	},
	{
		"id": 1,
		"nickname": "vasyan2",
		"first_name": "Vasya",
		"last_name": "Petrov",
		"birthday": "1990-12-14",
		"sex": 2,
		"city": "0",
		"phone": "+7 987 502-78-17",
		"email": null,
		"social_hidden": 0,
		"show_phone": 0,
		"copy_city": null,
		"videos_events": 0,
		"hide_age": 0,
		"contacts_hidden": 0,
		"filled": 1,
        "first_social": {
           "id": 1,
           "user_id": 1,
           "provider_user_id": "428393759",
           "provider": "vkontakte",
           "hide": 0,
           "page_id": "428393759",
           "created_at": "2017-05-18 15:25:13",
           "updated_at": "2017-06-19 11:56:09"
        }
	}
],
"from": 1,
"last_page": 1,
"next_page_url": null,
"path": "http://jumpin.dev/api/events/1/users",
"per_page": 10,
"prev_page_url": null,
"to": 2,
"total": 2


get /events/{id}/accept

результат: HTTP Status code 200


get /events/{id}/remove

результат: HTTP Status code 200


get /events/{id}/join

результат: HTTP Status code 200


get /events/{id}/leave

результат: HTTP Status code 200


get /users/({id}/me)

результат:
"user": {
    "id": 1,
    "nickname": "vasyan2",
    "first_name": "Vasya",
    "last_name": "Petrov",
    "birthday": "1990-12-14",
    "sex": 2,
    "city": "Санкт-Петербург, Россия",
    "phone": "+7 987 502-78-17",
    "email": null,
    "social_hidden": 0,
    "show_phone": 0,
    "copy_city": null,
    "videos_events": 0,
    "hide_age": 0,
    "contacts_hidden": 0,
    "first_social_id": 1,
    "filled": 1,
    "premium": 1,
    "photos": [
        {
            "id": 1,
            "model_id": 1,
            "model_type": "App\\User",
            "filename": "photos/users/1/I4Lz69xMd3zYfHUMO7ETy5BgOgE1b5eyldEVMd22.jpeg",
            "avatar": 0,
            "created_at": "2017-06-15 14:45:11",
            "updated_at": "2017-06-15 14:45:11"
        },
        {
            "id": 3,
            "model_id": 1,
            "model_type": "App\\User",
            "filename": "photos/users/1/bZBhYxDARW2TjGZn3XKuXbhG2KFptnJQrHcAuAB7.jpeg",
            "avatar": 0,
            "created_at": "2017-06-28 07:51:37",
            "updated_at": "2017-06-28 07:51:37"
        }
    ],
    "socials": [
        {
            "id": 1,
            "user_id": 1,
            "provider_user_id": "428393759",
            "provider": "vkontakte",
            "hide": 0,
            "social_name": "Social Name",
            "page_id": "428393759",
            "created_at": "2017-05-18 15:25:13",
            "updated_at": "2017-06-19 11:56:09",
            "url": "https://vk.com/id428393759"
        }
    ]
},
"data": {
    "events_count": {
        "own": 1,
        "archived": 0,
        "participant": 1
    },
    "age": "26 лет",
    "user_status": "me" //me - сам пользователь, friend - пользователь в друзьях, not_friend - не в друзьях
}


get /{users|events}/{id}/photos

результат:
"current_page": 1,
"data": [
	{
		"id": 2,
		"model_id": 1,
		"model_type": "App\\Event",
		"filename": "photos/events/1/pXnxgeJnZwisRhalDOPEXy6JWvVQpggKpsmcpAqJ.jpeg",
		"avatar": 0,
		"created_at": "2017-06-15 14:50:33",
		"updated_at": "2017-06-15 14:50:33"
	}
],
"from": 1,
"last_page": 1,
"next_page_url": null,
"path": "http://jumpin.dev/api/events/1/photos",
"per_page": 15,
"prev_page_url": null,
"to": 1,
"total": 1


get /{users|events}/photos/upload

'photos[]' => 'required|image|mimes:jpeg,png|max:5000'
'id' => если events то обязательно
результат: HTTP Status code 200


get /photos/delete/{id}

результат: HTTP Status code 200


get /messages

результат:
"current_page": 1,
"data": [
    {
        "id": 3,
        "sender_id": 1,
        "recipient_id": 3,
        "body": "gsdfgsdfg",
        "deleted_by": null,
        "created_at": "2017-06-22 00:00:00",
        "updated_at": "2017-06-22 00:00:00",
        "u": 3,
        "sender": {
            "id": 1,
            "nickname": "vasyan2",
            "first_name": "Vasya",
            "last_name": "Petrov",
            "birthday": "1990-12-14",
            "sex": 2,
            "city": "Санкт-Петербург, Россия",
            "phone": "+7 987 502-78-17",
            "email": null,
            "social_hidden": 0,
            "show_phone": 0,
            "copy_city": null,
            "videos_events": 0,
            "hide_age": 0,
            "contacts_hidden": 0,
            "filled": 1
        },
        "recipient": {
            "id": 3,
            "nickname": null,
            "first_name": "Archibald",
            "last_name": "Fox",
            "birthday": "1989-10-10",
            "sex": 2,
            "city": null,
            "phone": "+7 999 210-88-92",
            "email": null,
            "social_hidden": 0,
            "show_phone": 0,
            "copy_city": null,
            "videos_events": 0,
            "hide_age": 0,
            "contacts_hidden": 0,
            "filled": 0
        }
    }
],
"from": 1,
"last_page": 1,
"next_page_url": null,
"path": "http://jumpin.dev/api/messages",
"per_page": 10,
"prev_page_url": null,
"to": 1,
"total": 1


get /messages/{id}

результат:
"current_page": 1,
"data": [
    {
        "id": 1,
        "sender_id": 1,
        "recipient_id": 4,
        "body": "fsdfsdfsdfsdf",
        "created_at": "2017-06-23 00:00:00",
        "updated_at": "2017-06-23 00:00:00"
    },
    {
        "id": 5,
        "sender_id": 1,
        "recipient_id": 4,
        "body": "dfg dfg dfg dfg ",
        "created_at": "2017-06-22 00:00:00",
        "updated_at": "2017-06-22 00:00:00"
    }
],
"from": 1,
"last_page": 1,
"next_page_url": null,
"path": "http://jumpin.dev/api/messages/4",
"per_page": 6,
"prev_page_url": null,
"to": 2,
"total": 2


get /messages/suggests

результат:
"current_page": 1,
"data": [
    {
        "id": 2,
        "nickname": "Арчибальд",
        "first_name": "Mr",
        "last_name": "Fox",
        "birthday": "1994-07-23",
        "sex": 2,
        "city": "Тирасполь",
        "phone": "+7 921 938-32-69",
        "email": null,
        "social_hidden": 0,
        "show_phone": 0,
        "copy_city": null,
        "videos_events": 0,
        "hide_age": 0,
        "contacts_hidden": 0,
        "filled": 0,
        "first_social": {
           "id": 2,
           "user_id": 2,
           "provider_user_id": "158429303",
           "provider": "vkontakte",
           "hide": 0,
           "page_id": "158429303",
           "created_at": "2017-05-18 17:16:04",
           "updated_at": "2017-05-18 17:16:04"
        }
    },
    {
        "id": 5,
        "nickname": "user2",
        "first_name": "first2",
        "last_name": "last2",
        "birthday": "1993-09-02",
        "sex": 2,
        "city": "city2",
        "phone": "79875027812",
        "email": null,
        "social_hidden": 0,
        "show_phone": 0,
        "copy_city": null,
        "videos_events": 0,
        "hide_age": 0,
        "contacts_hidden": 0,
        "filled": 0,
        "first_social": {
           "id": 2,
           "user_id": 2,
           "provider_user_id": "158429303",
           "provider": "vkontakte",
           "hide": 0,
           "page_id": "158429303",
           "created_at": "2017-05-18 17:16:04",
           "updated_at": "2017-05-18 17:16:04"
        }
    }
],
"from": 1,
"last_page": 1,
"next_page_url": null,
"path": "http://jumpin.dev/api/messages/suggests",
"per_page": 10,
"prev_page_url": null,
"to": 2,
"total": 1


get /messages/delete/{id}, где id - id друга

результат: HTTP Status code 200


post /bind/{facebook|vkontakte|instagram}

'token' => {токен соц сети}
результат: HTTP Status code 200


get /photos/avatar/{id}

результат: HTTP Status code 200


	
В случае успеха всегда:
HTTP Status code 200

При ошибке нет прав доступа:
HTTP Status code 403
'error' => 'текст ошибки'

При ошибке валидации полей результат:
HTTP Status code 422
'{название поля}' => 'текст ошибки'

В случае непредвиденной ошибки:
HTTP Status code 500

У всех методов для вывода списка, есть параметр ?page={номер страницы}